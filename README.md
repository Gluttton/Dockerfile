Dockerfile
==========

Set of [Docker](https://www.docker.com) configuration files for personal purpose.

munkres-cpp	- Build environment for [munkres-cpp](https://gitlab.com/Gluttton/munkres-cpp).  
lacquer     	- Build environment for C++/Qt projects with some additional features.  
pslrk		- Build environment for [PslRK](https://gitlab.com/Gluttton/PslRK).  
qt5     	- Qt5 built from sources.  
webterminal     - Build environment for [WebTerminal](https://gitlab.com/Gluttton/WebTerminal). 


Notes
=====

```
docker build -t <name> --no-cache .
docker tag 000000000000 <user>/<name>:<tag>
docker login --username=<user>
docker push <user>/<name>:<tag>

docker ps -a
docker images -a
docker images -a -q
docker rmi -f $(docker images -a -q)
```
